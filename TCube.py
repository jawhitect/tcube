import clr
import sys

from System import Decimal
from System.Collections import *


sys.path.append(r"C:\Program Files\Thorlabs\Kinesis")

clr.AddReference("Thorlabs.MotionControl.DeviceManagerCLI")
clr.AddReference("Thorlabs.MotionControl.GenericMotorCLI")
clr.AddReference("Thorlabs.MotionControl.TCube.DCServoCLI")
clr.AddReference("Thorlabs.MotionControl.TCube.DCServo")
from Thorlabs.MotionControl.DeviceManagerCLI import *
from Thorlabs.MotionControl.GenericMotorCLI.Settings import *
from Thorlabs.MotionControl.TCube.DCServoCLI import *


'''
This class wraps .Net communications to Thorlabs TCube motors
'''
class TCube:
    device_list_result = DeviceManagerCLI.BuildDeviceList()

    '''
    Initialize object, set serial number of motor to which to connect.  
    '''
    def __init__(self, serial, name = ''):
        self.serial = serial
        if name:
            self.name = name
        else:
            self.name = 'device ' + str(self.serial)
        return

    '''
    To be called by with statement or XYZStage's __enter__ method; connects to and initializes the TCube motor
    '''
    def __enter__(self):
        self.device = TCubeDCServo.CreateTCubeDCServo(self.serial)
        self.device.Connect(self.serial)
        self.device.WaitForSettingsInitialized(5000)
        self.motorSettings = self.device.LoadMotorConfiguration(self.serial,
                                                                DeviceConfiguration.DeviceSettingsUseOptionType.UseFileSettings)
        self.device.StartPolling(250)

        self.device.EnableDevice()
        return self



    '''
    Called by the with statement or XYZStages' __exit__ method, closes the connection to motor.
    '''
    def __exit__(self, exception_type, exception_value, traceback):
        self.device.StopPolling()
        self.device.ShutDown()
        print("Connection to "+self.name+" successfully closed.")

    '''
    Moves the motor to its home position.
    '''
    def home(self):
        self.device.Home(60000)
        print(self.name + " homed.")

    '''
    Moves the motor to the given coordinate
    -------
    Input:
        coord - the coordinate to which to move the motor.
    '''
    def move(self, coord):
        self.device.MoveTo(Decimal(coord), 60000)
        print("Moved " + self.name + " to " + str(coord) + 'mm')

    def getPos(self):
        try:
            print('try request position')
            return self.device.requestPosition()
        except:
            print('try DevicePosition')
            return self.device.DevicePosition


