from TCube import TCube
import numpy as np

'''
This class manages three Thorlabs TCube motors used in an XYZ stage
'''
class TCubeXYZStage:
    '''
    Initializes the object. Takes in the serial numbers of each stage so that it can connect to them when the __enter method is called.
    ----------
    Inputs:
        Xserial - the serial number of the X stage
        Yserial - the serial number of the Y stage
        Zserial - the serial number of the Z stage
    '''
    def __init__(self, Xserial, Yserial, Zserial):
        self.xmot = TCube(Xserial, 'X')
        self.ymot = TCube(Yserial, 'Y')
        self.zmot = TCube(Zserial, 'Z')
        self.motors = (self.xmot, self.ymot, self.zmot)

        # These data members help update the user on scanning progress
        self._xyzScanning = False #if set to True, xyScan method knows it is a subscan
        self.scan_measurements = 0 # the number of measurements taken so far in the scan
        self.scan_total_measurements = None # the number of measurements the scan will need to take to complete
    '''
    Called by with statement: connects to the motors.
    '''
    def __enter__(self):
        for mot in self.motors: mot.__enter__()
        return self
    '''
    Called by the with statement: ensures that connections with the motors are closed.
    '''
    def __exit__(self, exception_type, exception_value, traceback):
        for mot in self.motors: mot.__exit__(exception_type, exception_value, traceback)

    '''
    Move the stage to position x, y z
    ----------
    Inputs:
        x - the x position in mm
        y - the y position in mm
        z - the z position in mm
    '''
    def move(self, x, y, z):
        pos = (x, y, z)
        for i, mot in enumerate(self.motors): mot.move(pos[i])

    '''
    Implements an xyz scan acquiring and saving a measurement at each grid location
    ----------
    Inputs:
        measure - the measurement function implemented and passed in by the user. Must return a numeric
        x_start - x starting position of the scan
        x_end - x ending position of the scan
        x_int - x interval between scanning points
        y_start - y starting position of the scan
        y_end - y ending position of the scan
        y_int - y interval between scanning points
        z_start - z starting position of the scan
        z_end - z ending position of the scan
        z_int - z interval between scanning points
    Output:
        3 dimensional numpy array with measurements at each scanning position
    '''
    def xyzScan(self, measure, x_start, x_end, x_int, y_start, y_end, y_int, z_start, z_end, z_int):
        xcoords = np.arange(x_start, x_end, x_int)
        ycoords = np.arange(y_start, y_end, y_int)
        zcoords = np.arange(z_start, z_end, z_int)
        result = np.zeros([len(xcoords), len(ycoords), len(zcoords)])

        # keep track of how many scan measurements have been taken to inform the user of progress
        self.scan_total_measurements = np.size(result)
        self.scan_measurements = 0

        #to avoid hysteresis, move z position a little bast the start befor beginning scan
        z_init = 0.1*(z_start-z_end)/np.abs(z_start-z_end) + z_start
        self.zmot.move(z_init)

        for k, z in enumerate(zcoords):
            self.zmot.move(z)
            print("z position " + str(z) + " of " + str(len(zcoords)))
            (result[:, :, k], coords) = self.xyScan(measure, x_start, x_end, x_int, y_start, y_end, y_int)
        return result, (xcoords, ycoords, zcoords)

    '''
    Implements ab xy scan acquiring and saving a measurement at each grid location
    ----------
    Inputs:
        measure - the measurement function implemented and passed in by the user. Must return a numeric
        x_start - x starting position of the scan
        x_end - x ending position of the scan
        x_int - x interval between scanning points
        y_start - y starting position of the scan
        y_end - y ending position of the scan
        y_int - y interval between scanning points
    Output:
        2 dimensional numpy array with measurements at each scanning position
    '''
    def xyScan(self, measure, x_start, x_end, x_int, y_start, y_end, y_int):
        xcoords = np.arange(x_start, x_end, x_int)
        ycoords = np.arange(y_start, y_end, y_int)
        result = np.zeros([len(xcoords), len(ycoords)])

        if not self._xyzScanning:
            self.scan_total_measurements = np.size(result)
            self.scan_measurements = 0

        # to avoid hysteresis, move the stage a little farther than start coordinates before a row scan

        x_init = 0.1*(x_start-x_end)/np.abs(x_start-x_end) + x_start
        y_init = 0.1 * (y_start - y_end) / np.abs(y_start - y_end) + y_start
        self.xmot.move(x_init)
        for i, x in enumerate(xcoords):
            print("X move:")
            self.xmot.move(x)
            self.ymot.move(y_init)
            for j, y in enumerate(np.arange(y_start, y_end, y_int)):
                self.ymot.move(y)
                result[i, j] = measure()
                self.scan_measurements += 1
                print('Scanned %d of %d points.'%(self.scan_measurements, self.scan_total_measurements))
        return result, (xcoords, ycoords)

    '''
    This method finds the optimal focus that maximizes the value of the measure function passed in.
    '''
    def findOptimalFocus(self, measure, x_start, x_end, x_int, y_start, y_end, y_int, z_start, z_end, z_int):
        iteration = 1
        lastBestSignal = -1
        bestSignal = 0
        max_resolution = 0.00005 #mm
        xyzres_maxed = [False, False, False]
        while(bestSignal > lastBestSignal):
            results, coords = self.xyzScan(measure, x_start, x_end, x_int, y_start, y_end, y_int, z_start, z_end, z_int)

            #get indices of best focus
            ind = np.unravel_index(np.argmax(results, axis=None), results.shape)

            #get coordinates of best focus
            xmax = coords[0][ind[0]]
            ymax = coords[1][ind[1]]
            zmax = coords[2][ind[2]]
            '''
            for i, res in enumerate((x_int, y_int, z_int)):
                if res <max_resolution:
                    xyzres_maxed[i] = True
            '''

            #perform zoom

        return

    '''
    Helper Method for findOptimalFocus. Decides range to scan over an axis for the next iteration.
    -----
    Inputs:
        start - the start positon along the axis for the last scan
        end - the end position along the axis for the last scan
        interval - the interval scanned along the axis for the last scan
        maxcoord - the coordinate of the best signal observed in the last scan
    Returns:
        newstart - coordinate where to start the next scanning iteration
        newend -coordinate where to end the next scanning iteration
        newinterval - spacing between 
    '''
    def _axisZoom(self, start, end, interval, maxcoord):

        # find the range
        range = end - start

        #if the maximum is on the boundary of the previous scan, then repeat scan of same range and interval around the maximum.
        if maxcoord == start or maxcoord == end:
            newstart = maxcoord - range / 2
            newend = maxcoord - range / 2
            return newstart, newend, interval
        # otherwise we will zoom in reducing the interval by 1/2, and zoom into the area with 2 of the previous intervals
        # around the previous maximum
        else:
            newinterval = interval/4
            new_half_range = interval*1.25

        # if this operation reduces the interval to less than the maximum resolution of the stage (~0.00005 mm), then increase
        # the interval to 0.00005 mm
        if newinterval < 0.00005:
            newinterval = 0.00005
        newstart = maxcoord - new_half_range
        newend = maxcoord + new_half_range
        return newstart, newend, newinterval







